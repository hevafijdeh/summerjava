package kz.aitu.oop.practice.practice4;

import java.util.List;

public class CalculatePrice {
    public void Calculate(Aquarium Aquarium){
        int price = 0;
        List<Animal> animals = Aquarium.getAnimals();
        for(Animal animal : animals){
            price+=animal.getPrice();
        }
        System.out.println("The price for your custom Aquarium is $" + price + '.');
    }
}
