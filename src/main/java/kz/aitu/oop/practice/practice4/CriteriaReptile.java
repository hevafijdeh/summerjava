package kz.aitu.oop.practice.practice4;

import java.util.ArrayList;
import java.util.List;

public class CriteriaReptile implements Criteria{
    @Override
    public List<Animal> meetCriteria(List<Animal> animals) {
        List<Animal> animals1 = new ArrayList<Animal>();
        for(Animal animal : animals){
            if(animal instanceof Reptile){
                animals1.add(animal);
            }
        }
        return animals1;
    }
}
