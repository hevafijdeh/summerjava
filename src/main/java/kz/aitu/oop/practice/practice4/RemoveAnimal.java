package kz.aitu.oop.practice.practice4;

import java.util.List;

public class RemoveAnimal {
    public void Remove(Aquarium aquarium, int id){
        if (aquarium.getAnimals().size()>0 ){
            List<Animal> newanimals = aquarium.getAnimals();
            newanimals.remove(id);
            aquarium.setAnimals(newanimals);
        }
    }
}
