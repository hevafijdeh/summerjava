package kz.aitu.oop.practice.practice4;

import javax.xml.crypto.Data;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBconnection {
    private DataBase database;
    private Connection con;
    private static DBconnection instance;

    public Connection getCon() {

        return con;
    }

    public static DBconnection getInstance(){
        if(DBconnection.instance == null){
            DataBase database = new DataBase();
            DBconnection.instance = new DBconnection(database);
        }
        return instance;
    }
    private DBconnection(DataBase database){
        this.database = database;
        Connection con2 = null;
        try {
            con2 = DriverManager.getConnection(database.getUrl(), database.getUser(), database.getPassword());
            this.con = con2;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        this.con = con2;
    }
    public void setCon(Connection con) {
        this.con = con;
    }
    public void setCon(){
        Connection con2;
        try {
            con2 = DriverManager.getConnection(database.getUrl(), database.getUser(), database.getPassword());
            this.con = con2;
        } catch (SQLException e) {
            e.printStackTrace();

        }
    }

}
