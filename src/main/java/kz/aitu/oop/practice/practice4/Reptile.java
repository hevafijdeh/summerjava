package kz.aitu.oop.practice.practice4;

public class Reptile extends Animal {

    public Reptile(int weight, int price, String name) {
        super(weight, price, name);
    }


    @Override
    public int getWeight() {
        return super.getWeight();
    }

    @Override
    public void setWeight(int weight) {
        super.setWeight(weight);
    }

    @Override
    public int getPrice() {
        return super.getPrice();
    }

    @Override
    public void setPrice(int price) {
        super.setPrice(price);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }
    @Override
    public String toString() {
        return "Reptile{" +
                super.toString() +
                '}';
    }
    @Override
    public void Sound(){
        System.out.println(getName() + " growls!");
    }
}
