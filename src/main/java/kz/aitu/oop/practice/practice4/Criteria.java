package kz.aitu.oop.practice.practice4;

import java.util.List;

public interface Criteria {
    public List<Animal> meetCriteria(List<Animal> animals);
}
