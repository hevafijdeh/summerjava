package kz.aitu.oop.practice.practice4;

public class DataBase {

    private String url = "jdbc:mysql://localhost:3306/java?useLegacyDatetimeCode=false&serverTimezone=UTC";
    private String user = "root";
    private String password  = "";

    /*
    * SQL DDL
    *
    * CREATE TABLE animals(
    * anim_id decimal(3),
    * anim_name varchar(20),
    * price decimal(100),
    * weight decimal(100),
    * type_anim decimal(20)
    * );
    *
    */

    public DataBase(){
        this.url = url;
        this.user = user;
        this.password = password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public String getUrl() {
        return url;
    }

    public String getUser() {
        return user;
    }

}
