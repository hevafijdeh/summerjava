package kz.aitu.oop.practice.practice4;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void Menu(){
        System.out.println("Choose a querry to execute");
        System.out.println("(1) Display all animals in your aquarium");
        System.out.println("(2) Edit aquarium");
        System.out.println("(3) Buy your all the animals from your custom aquarium");
        System.out.println("(4) Add new animals to shop (Admin only!)");
        System.out.println("(5) Show shops stock");
        System.out.println("(0) Exit");
    }

    public static void Querry2(){
        System.out.println("Choose a querry to execute");
        System.out.println("(1) Add new animal ");
        System.out.println("(2) Remove one of the animals");
        System.out.println("(3) Remove all of the animals from aquarium");
        System.out.println("(0) Back to main menu");
    }
    public static void Querry2_1(){
        System.out.println("Choose type of animal you want:");
        System.out.println("(1) Fish");
        System.out.println("(2) Reptile");
        System.out.println("(0) Cancel");
    }
    public static void Querry3(){
        System.out.println("Confirm if you really want to make a purchase");
        System.out.println("(1) Confirm");
        System.out.println("(2) Deny");
    }
    private static void go()
    {
        System.out.println("Press Enter to continue...");
        try
        {
            System.in.read();
        }
        catch(Exception e)
        {}
    }

    public static void main(String[] args){
        DBconnection con = DBconnection.getInstance();
        CalculatePrice calculate = new CalculatePrice();
        ClearAquarium clear = new ClearAquarium();
        RemoveAnimal remove = new RemoveAnimal();
        PrintList print = new PrintList();
        Shop shop = new Shop();
        Aquarium aquarium = new Aquarium();
        Scanner in = new Scanner(System.in);

        try {
            System.out.println("Connecting to Database...");
            Statement start = con.getCon().createStatement();
            ResultSet resultSet = start.executeQuery("Select * from Animals");
            while(resultSet.next()){
                if(resultSet.getInt(5) == 1){
                    Animal animal = new Fish(resultSet.getInt(4) , resultSet.getInt(3), resultSet.getString(2));
                    shop.addAnimal(animal);
                }
                if(resultSet.getInt(5) == 2){
                    Animal animal = new Reptile(resultSet.getInt(4) , resultSet.getInt(3), resultSet.getString(2));
                    shop.addAnimal(animal);
                }
            }
            System.out.println("Connected successfully!");
            int q = 1;
            while (q!=0){
                try {
                    Menu();
                    q = in.nextInt();
                    if(q==1){
                        print.Print(aquarium.getAnimals());
                    }
                    else if (q==2){
                        Querry2();
                        int q2 = in.nextInt();
                        if(q2 == 1){
                            Querry2_1();
                            int q2_1 = in.nextInt();
                            if(q2_1 == 1){
                                Criteria prec = new CriteriaFish();
                                List<Animal> temp = prec.meetCriteria(shop.getStock());
                                print.Print(temp);
                                System.out.println("Choose one for Aquarium...");
                                int f1 = in.nextInt();
                                if (f1>temp.size() || f1<1){
                                    throw new InputMismatchException();
                                }else{
                                    aquarium.addAnimal(temp.get(f1-1));
                                    System.out.println("Successfully added new animal!");
                                }
                            }
                            else if(q2_1 == 2){
                                Criteria semi = new CriteriaReptile();
                                List<Animal> temp = semi.meetCriteria(shop.getStock());
                                print.Print(temp);
                                System.out.println("Choose one for Aquarium...");
                                int f1 = in.nextInt();
                                if (f1>temp.size() || f1<1){
                                    throw new InputMismatchException();
                                }else{
                                    aquarium.addAnimal(temp.get(f1-1));
                                    System.out.println("Successfully added new animal!");
                                }
                            }
                            else if(q2_1 == 0){

                            }
                            else{
                                throw new InputMismatchException();
                            }
                        }
                        else if(q2 == 2){
                            print.Print(aquarium.getAnimals());
                            System.out.println("Choose one animal to be removed");
                            int f = in.nextInt();
                            if(f>aquarium.getAnimals().size() || f<1){
                                throw new InputMismatchException();
                            }
                            else{
                                remove.Remove(aquarium, f-1);
                                System.out.println("Successfully removed animal!");
                            }
                        }
                        else if(q2 == 3){
                            clear.Clearing(aquarium);
                            System.out.println("Successfully removed all of the animals!");
                        }
                        else if(q2 == 0){

                        }
                        else {
                            throw new InputMismatchException();
                        }
                    }
                    else if (q==3){
                        calculate.Calculate(aquarium);
                        Querry3();
                        int d = in.nextInt();
                        if(d == 1){
                            System.out.println("Successfully made a purchase!");
                            for(Animal j : aquarium.getAnimals()){
                                j.Sound();
                            }
                            clear.Clearing(aquarium);
                        }else if(d==2){
                            System.out.println("Purchase was cancelled");
                        }else{
                            throw new InputMismatchException();
                        }
                    }
                    else if (q==4){
                        System.out.println("Confirm that you are admin by writing password:");
                        String pass = in.next();
                        if(pass.equals("12345")){
                            System.out.println("Login Successfully!");
                            System.out.println("Please write a name for a new animal");
                            String name = in.next();
                            System.out.println("Write the price in USD for a new animal");
                            int price = in.nextInt();
                            System.out.println("Write the weight in grams for a new animal (integers only)");
                            int weight = in.nextInt();
                            System.out.println("Write 1 if " + name + " is a fish");
                            System.out.println("Write 2 if " + name + " is a reptile");
                            int r = in.nextInt();
                            Animal newanimal;
                            if(r==1){
                                newanimal = new Fish(weight, price, name);
                            }else if(r==2){
                                newanimal = new Reptile(weight, price, name);
                            }else{
                                throw new InputMismatchException();
                            }
                            System.out.println("Adding new animal to Database...");
                            Statement stmt = con.getCon().createStatement();
                            ResultSet rowres = stmt.executeQuery("Select * from animals");
                            int id = 0;
                            while (rowres.next()){ //a loop to determine last row, so that we know what will be the next id we need to use
                                id++;
                            }
                            id++;
                            String querry = "Insert into animals(anim_id, anim_name, price, weight, type_anim) values (" + id + ", '"+ name + "', " + price
                                    + ", " + weight + ", " + r + ")";
                            Statement stmt2 = con.getCon().createStatement();
                            stmt2.executeUpdate(querry);
                            shop.addAnimal(newanimal);
                            System.out.println("Shop stock was updated.");
                        }else{
                            System.out.println("Incorrect password!");
                        }
                    }
                    else if(q==5){
                        print.Print(shop.getStock());
                    }
                    else if(q==0){

                    }

                    else{
                        throw new InputMismatchException();
                    }
                }catch (InputMismatchException ime){
                    System.out.println("Wrong Querry!");
                    in.next();
                    q = 1;
                }finally {
                    go();
                }
            }

        }catch (SQLException sqle){
            System.out.println("Failed to connect to Database");
        }
        System.out.println("Thank you for visiting the shop!");

    }
}
