package kz.aitu.oop.practice.practice4;

import java.util.ArrayList;
import java.util.List;

public class Shop {
    private List<Animal> stock = new ArrayList();

    public Shop(){

    }

    public Shop(List<Animal> stock) {
        this.stock = stock;
    }

    public List<Animal> getStock() {
        return stock;
    }

    public void setStock(List<Animal> stock) {
        this.stock = stock;
    }

    public void addAnimal(Animal animal){
        this.stock.add(animal);
    }

    @Override
    public String toString() {
        return "Shop{" +
                "stock=" + stock +
                '}';
    }
}
