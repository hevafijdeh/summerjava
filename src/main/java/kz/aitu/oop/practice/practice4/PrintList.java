package kz.aitu.oop.practice.practice4;

import java.util.List;

public class PrintList {
    public void Print(List<Animal> animals){
        if(animals.size()<1) System.out.println("There is no animals yet...");
        else
        for(Animal animal : animals){
            String type = "";
            if(animal instanceof Fish) type ="(Fish)";
            if(animal instanceof Reptile) type = "(Reptile)";

            System.out.println("(" + (animals.indexOf(animal) + 1) + ") " + animal.getName() + " [price = $" + animal.getPrice() +
                    ", weight = " + animal.getWeight() + " grams] " + type);
        }
    }
}
