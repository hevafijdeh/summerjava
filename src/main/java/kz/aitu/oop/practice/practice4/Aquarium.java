package kz.aitu.oop.practice.practice4;

import java.util.ArrayList;
import java.util.List;

public class  Aquarium{
    private List<Animal> animals = new ArrayList<Animal>();

    public Aquarium(){

    }

    public Aquarium(List<Animal> animals) {
        this.animals = animals;
    }

    public List<Animal> getAnimals() {
        return animals;
    }

    public void setAnimals(List<Animal> animals) {
        this.animals = animals;
    }

    public void addAnimal(Animal animal){
        this.animals.add(animal);
    }
}
