package kz.aitu.oop.practice.practice4;

public abstract class Animal implements AnimalSound{ //inheritance

    private int weight; //encapsulation
    private int price;
    private String name;

    public Animal(int weight, int price, String name) {
        this.weight = weight;
        this.price = price;
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "weight=" + weight +
                ", price=" + price +
                ", name='" + name + '\'' +
                '}';
    }
}
