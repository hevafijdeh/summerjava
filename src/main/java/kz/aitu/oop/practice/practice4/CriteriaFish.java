package kz.aitu.oop.practice.practice4;

import java.util.ArrayList;
import java.util.List;

public class CriteriaFish implements Criteria {
    @Override
    public List<Animal> meetCriteria(List<Animal> animals) {
        List<Animal> animals1 = new ArrayList<Animal>();
        for(Animal animal : animals){
            if(animal instanceof Fish){
                animals1.add(animal);
            }
        }
        return animals1;
    }
}
