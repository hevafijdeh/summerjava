package kz.aitu.oop.examples.task2;

public class MySpecialString {
    private int[] values;

    public MySpecialString(int[] values){
        this.values = values;
    }

    public int[] getValues() {
        return values;
    }

    public void setValues(int[] values) {
        this.values = values;
    }

    public int length(){
        return this.values.length;
    }

    public int valueAt(int position){
        if(values.length<position || position<1){
            return -1;
        }
        else{
            return values[position-1];
        }
    }

    public boolean contains(int value){
        for(int a : values){
            if(a == value) return true;
        }
        return false;
    }
    public int count(int value){
        int cnt = 0;
        for(int a: values){
            if(a == value){
                cnt++;
            }
        }
        return cnt;
    }
    public void Print(){
        for(int a : this.values){
            System.out.print(a);
            System.out.print(" ");
        }
        System.out.println();
    }

}
