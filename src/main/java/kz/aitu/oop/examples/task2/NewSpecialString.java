package kz.aitu.oop.examples.task2;


public class NewSpecialString {
    private String[] values;

    public NewSpecialString(String[] values){
        this.values = values;
    }

    public String[] getValues() {
        return values;
    }

    public void setValues(String[] values) {
        this.values = values;
    }

    public int length(){
        return this.values.length;
    }

    public String valueAt(int position){
        if(values.length<position || position<1){
            return "-1";
        }
        else{
            return values[position-1];
        }
    }

    public boolean contains(String value){
        for(String a : values){
            if(a.equals(value)) return true;
        }
        return false;
    }

    public int count(String value){
        int cnt = 0;
        for(String a: values){
            if(a.equals(value)){
                cnt++;
            }
        }
        return cnt;
    }

    public void Print(){
        for(String a : this.values){
            System.out.print(a);
            System.out.print(" ");
        }
        System.out.println();
    }
}
