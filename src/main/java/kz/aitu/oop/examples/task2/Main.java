package kz.aitu.oop.examples.task2 ;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        //part 1
        System.out.println("Part 1");
        int[] array = {1, 2 , 3 , 10 ,112, 2 , 2};
        MyString str = new MyString(array);
        System.out.println(str.contains(13));
        System.out.println(str.contains(1));
        System.out.println(str.count(2));
        System.out.println(str.length());
        System.out.println(str.valueAt(9));
        System.out.println(str.valueAt(7));
        str.Print();

        //part 2
        System.out.println("Part 2");
        Scanner in = new Scanner(System.in);
        System.out.println("Please write a name for new file:");
        String filename = in.nextLine();
        if(filename.isEmpty()){
            System.exit(0);
        }
        System.out.println("Successfully created file");
        File newfile = new File(filename + ".txt");
        PrintWriter write = new PrintWriter(filename + ".txt");
        for(int a: str.getValues()){
            write.println(a);
        }
        write.close();

        //part 3
        System.out.println("Part 3");
        Scanner reader = new Scanner(newfile);
        int size = 0;
        ArrayList<String> arrayList = new ArrayList<String>();
        while (reader.hasNextLine()){
            arrayList.add(reader.nextLine());
            size++;
        }
        int i = 0;
        int array2[] = new int[size];
        for(String a : arrayList){
            array2[i] = Integer.parseInt(a);
            i++;
        }
        MyString str2 = new MyString(array2);
        //part 4
        System.out.println("Part 4");
        str.checkEqual(str2);
        int[] array3 = {1,2,3};
        MyString str3 = new MyString(array3);
        str.checkEqual(str3);

        //part 5
        System.out.println("Part 5");
        int array4[] = {1,2,3,4,5,6};
        MySpecialString spclStr = new MySpecialString(array4);
        System.out.println(spclStr.contains(1));
        System.out.println(spclStr.count(6));
        System.out.println(spclStr.length());
        System.out.println(spclStr.valueAt(3));
        spclStr.Print();

        //part 6
        System.out.println("Part 6");
        String[] array5 = {"arr", "1", "2", "333", "2"};
        NewSpecialString newstr = new NewSpecialString(array5);
        System.out.println(newstr.contains("1"));
        System.out.println(newstr.count("2"));
        System.out.println(newstr.length());
        System.out.println(newstr.valueAt(3));
        newstr.Print();
    }
}
